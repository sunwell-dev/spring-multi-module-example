# Spring Boot Multi-Module Project Example

## Overview
This project is to demonstrate how to use Maven to build Spring Boot multi-module project.

There are two modules in this project:
- **library**: A simple module that use spring-data and JPA. It contains <code>@Entity</code>, <code>@Service</code>, and <code>@Repository</code> classes.
- **application**: The main module that use the above module (library) and serve as application entry-point. You'll find the <code>@SpringBootApplication</code> in this module.

## Requirements
To run this project, you will need:
1. JDK 11 or higher (should work with JDK 8)
2. Maven 3.8 or higher (haven't tested with previous versions)
3. Spring Boot 2.5.9 RELEASE or higher (should be able to run with previous versions also)
4. PostgreSQL 11 or higher (should be able to run with previous version such as 9.6)
5. A database with the following table (DDL SQL):

<pre>
CREATE TABLE countries (
    isocode CHAR(2) PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);</pre>

7. Create application.properties file in application module and specify how to connect to your database.

## References
1. [https://spring.io/guides/gs/multi-module/](https://spring.io/guides/gs/multi-module/)
2. [https://www.baeldung.com/spring-boot-multiple-modules](https://www.baeldung.com/spring-boot-multiple-modules)
