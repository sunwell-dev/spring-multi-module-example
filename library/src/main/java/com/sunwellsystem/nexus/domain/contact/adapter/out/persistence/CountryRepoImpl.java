/*
 * CountryRepoImpl.java
 *
 * Created on Feb 22, 2022, 15.17
 */
package com.sunwellsystem.nexus.domain.contact.adapter.out.persistence;

import com.sunwellsystem.nexus.domain.adapter.out.persistence.DefaultSpringRepositoryImpl;
import com.sunwellsystem.nexus.domain.contact.entity.Country;
import com.sunwellsystem.nexus.domain.contact.repository.CountryRepo;
import org.springframework.stereotype.Repository;

/**
 * @author Irfin A., Davin Y., Fiona A.
 */
@Repository
public class CountryRepoImpl extends DefaultSpringRepositoryImpl<Country, String> implements CountryRepo
{
    private final CountrySpringRepo springRepo;

    public CountryRepoImpl(CountrySpringRepo csr)
    {
        super(csr);
        springRepo = csr;
    }
}