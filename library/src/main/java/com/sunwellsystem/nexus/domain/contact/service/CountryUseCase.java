/*
 * CountryUseCase.java
 *
 * Created on Feb 22, 2022, 15.30
 */
package com.sunwellsystem.nexus.domain.contact.service;

import com.sunwellsystem.nexus.domain.contact.entity.Country;
import com.sunwellsystem.nexus.domain.contact.repository.CountryRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Irfin A., Davin Y., Fiona A.
 */
@Service
public class CountryUseCase
{
    private CountryRepo repo;

    public CountryUseCase(CountryRepo _repo)
    {
        repo = _repo;
    }

    @Transactional(rollbackOn = Exception.class)
    public Country create(Country c)
    {
        repo.save(c);
        return c;
    }

    public Iterable<Country> getAll()
    {
        return repo.findAll();
    }
}
