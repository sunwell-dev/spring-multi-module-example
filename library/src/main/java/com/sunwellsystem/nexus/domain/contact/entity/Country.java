/*
 * Country.java
 *
 * Created on Feb 22, 2022, 15.12
 */
package com.sunwellsystem.nexus.domain.contact.entity;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.StringJoiner;

/**
 * @author Bello Cappolo
 * @author Christian Hardy
 * @author Irfin A
 */
@Entity
@Table(name = "countries")
public class Country
{
    @Id
    @Column(name = "isocode")
    private String isoCode;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "created_at", updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createdAt;

    @PrePersist
    private void onCreate()
    {
        createdAt = LocalDateTime.now();
    }

    public String getIsoCode()
    {
        return isoCode;
    }

    public void setIsoCode(String isoCode)
    {
        this.isoCode = isoCode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public LocalDateTime getCreatedAt()
    {
        return createdAt;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(",\n", Country.class.getSimpleName() + "[", "]")
                .add("isoCode='" + isoCode + "'")
                .add("name='" + name + "'")
                .add("createdAt=" + createdAt)
                .toString();
    }
}
