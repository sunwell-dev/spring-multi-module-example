/*
 * BasicEntityRepository.java
 *
 * Created on Feb 22, 2022, 15.26
 */
package com.sunwellsystem.nexus.domain.common.repository;

import java.util.Optional;

/**
 * @author Irfin A.
 */
public interface BasicEntityRepository<T, ID>
{
    <S extends T> S save(S entity);

    Optional<T> findById(ID id);

    boolean isExistsById(ID id);

    Iterable<T> findAll();

    default void deleteById(ID id)
    {
        findById(id).ifPresent(this::delete);
    }

    default void delete(T entity)
    {
        throw new RuntimeException("Not implemented yet");
    }
}
