/*
 * CountrySpringRepo.java
 *
 * Created on Feb 22, 2022, 15.17
 */
package com.sunwellsystem.nexus.domain.contact.adapter.out.persistence;

import com.sunwellsystem.nexus.domain.contact.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Irfin A., Davin Y., Fiona A.
 */
@Repository
interface CountrySpringRepo extends JpaRepository<Country, String>
{
}
