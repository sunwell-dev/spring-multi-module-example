/*
 * DefaultSpringRepositoryImpl.java
 *
 * Created on Feb 22, 2022, 15.19
 */
package com.sunwellsystem.nexus.domain.adapter.out.persistence;

import com.sunwellsystem.nexus.domain.common.repository.BasicEntityRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * @author Irfin A.
 */
public class DefaultSpringRepositoryImpl<T, ID> implements BasicEntityRepository<T, ID>
{
    private final CrudRepository<T, ID> springRepo;

    public DefaultSpringRepositoryImpl (CrudRepository<T, ID> crudRepo)
    {
        springRepo = crudRepo;
    }

    @Override
    public <S extends T> S save(S entity)
    {
        return springRepo.save(entity);
    }

    @Override
    public Optional<T> findById(ID id)
    {
        return springRepo.findById(id);
    }

    @Override
    public boolean isExistsById(ID id)
    {
        return springRepo.existsById(id);
    }

    @Override
    public Iterable<T> findAll()
    {
        return springRepo.findAll();
    }
}

