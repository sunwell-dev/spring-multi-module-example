/*
 * CountryRepo.java
 *
 * Created on Feb 22, 2022, 15.15
 */
package com.sunwellsystem.nexus.domain.contact.repository;


import com.sunwellsystem.nexus.domain.common.repository.BasicEntityRepository;
import com.sunwellsystem.nexus.domain.contact.entity.Country;

/**
 * @author Irfin A., Davin Y., Fiona A.
 */
public interface CountryRepo extends BasicEntityRepository<Country, String>
{
}
