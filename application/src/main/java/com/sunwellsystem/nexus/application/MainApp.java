/*
 * MainApp.java
 *
 * Created on Feb 22, 2022, 15.52
 */
package com.sunwellsystem.nexus.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Irfin A., Davin Y., Fiona A.
 */
@SpringBootApplication(scanBasePackages = {"com.sunwellsystem.nexus"})
@EnableJpaRepositories(basePackages = {"com.sunwellsystem.nexus.domain"})
@EntityScan(basePackages = {"com.sunwellsystem.nexus.domain"})
public class MainApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(MainApp.class, args);
    }
}
