/*
 * CountryCtrl.java
 *
 * Created on Feb 22, 2022, 15.39
 */
package com.sunwellsystem.nexus.application.http.contact.controller;

import com.sunwellsystem.nexus.domain.contact.entity.Country;
import com.sunwellsystem.nexus.domain.contact.service.CountryUseCase;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Irfin A., Davin Y., Fiona A.
 */
@RestController
@RequestMapping("/api/country")
public class CountryCtrl
{
    private CountryUseCase useCase;

    public CountryCtrl(CountryUseCase _countryUseCase)
    {
        useCase = _countryUseCase;
    }

    @GetMapping
    public Iterable<Country> getAll()
    {
        return useCase.getAll();
    }
}
